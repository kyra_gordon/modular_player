extends Node

var body
var camera
var lean

export (bool) var enabled = true
export (float) var lean_amount = 30
export (float) var lean_RPM = 5
export (bool) var bounded = true
export (String) var bind_left = "lean left"
export (String) var bind_right = "lean right"

func _ready():
	body = get_parent()
	for i in body.get_children():
		if i.name == "FP cam module":
			camera = i
	lean = camera.get_child(0)
	pass

var LB = 0
var RB = 0
func _process(delta):
	var b_axis = body.global_transform.basis.z
	if enabled:
		if Input.is_action_pressed(bind_left):
			if bounded:
				lean.rotation_degrees = lean.rotation_degrees.linear_interpolate(Vector3(0,0,lean_amount),delta*lean_RPM)
			else:
				LB = clamp(LB+delta,0,1)
		else:
			LB = clamp(LB-delta,0,1)
		if Input.is_action_pressed(bind_right):
			if bounded:
				lean.rotation_degrees = lean.rotation_degrees.linear_interpolate(Vector3(0,0,-lean_amount),delta*lean_RPM)
			else:
				RB = clamp(RB+delta,0,1)
			pass
		else:
			RB = clamp(RB-delta,0,1)
		if !Input.is_action_pressed(bind_right) and !Input.is_action_pressed(bind_left) and bounded:
			lean.rotation_degrees = lean.rotation_degrees.linear_interpolate(Vector3(),delta*lean_RPM)
		if !bounded:
			body.rotate(b_axis, LB*delta*lean_RPM)
			body.rotate(b_axis, -RB*delta*lean_RPM)
			
			pass
	pass

var AI_init_state = 0 #0:uninitialised, 1:initialised, 2:initialised+disabled

func AI(_delta):
	pass
