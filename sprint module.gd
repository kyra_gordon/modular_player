extends Node

export (bool) var enabled = true
export (bool) var toggle_sprint = false

onready var body = get_parent()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if enabled:
		if Input.is_action_pressed("sprint") and !toggle_sprint:
			body.move_mode = "sprint"
		else:
			body.move_mode = "walk"
		if Input.is_action_just_pressed("sprint") and toggle_sprint:
			if body.move_mode == "sprint":
				body.move_mode = "walk"
			elif body.move_mode == "walk":
				body.move_mode = "sprint"
	pass


var AI_init_state = 0 #0:uninitialised, 1:initialised, 2:initialised+disabled

func AI(_delta):
	pass
