extends Node

var body

export (bool) var enabled = true
export (float) var impulse = 5
var db = 0
export (int) var airjumps = 0
export (bool) var use_airspeed = false
export (Array) var airspeed = [15,0,30]#acel, decel, top
export (String) var bind = "movement_jump"
var moveBuffer
var airjumps_left = airjumps
var jumping = false
var jvec

func _ready():
	body = get_parent()
	body.movement_modes["airspeed"] = airspeed
	pass

func call(delta):
	jvec = body.global_transform.basis.y*impulse
	if body.is_on_floor():
		airjumps_left = airjumps
		if body.move_mode == "airspeed" and use_airspeed:
			body.move_mode = moveBuffer
	if enabled:
		if Input.is_action_pressed(bind) and body.is_on_floor() or airjumps > 0:
				if use_airspeed:
					moveBuffer = body.move_mode
					body.move_mode = "airspeed"
				jump()
				airjumps -=1
	pass

func jump():
#	body.velbuffer+=jvec
	body.velocity+=jvec

var AI_init_state = 0 #0:uninitialised, 1:initialised, 2:initialised+disabled

func AI(_delta):
	pass
