extends Spatial

var body

onready var pivot = $"lean/pivot"

onready var camera = $"lean/pivot/Camera"

export (bool) var rotate_body_x = false

export (bool) var rotate_body_y = true

export (float) var look_speed_horizontal = 1
export (float) var look_speed_vertical = 1
export (float) var mouse_sensitivity_horizontal = .5
export (float) var mouse_sensitivity_vertical = .5
export (bool) var vertical_inverted = false
export (bool) var horizontal_inverted = false
export (float) var max_rot_x = 90
export (float) var min_rot_x = -90
export (float) var max_rot_y = 90
export (float) var min_rot_y = -90
export (bool) var limit_x = true
export (bool) var limit_y = false
export (String) var bind_up = "look_up"
export (String) var bind_down = "look_down"
export (String) var bind_left = "look_left"
export (String) var bind_right = "look_right"
var rotatepointx
var rotatepointy

var mouseMotion = Vector2()

func _ready():
#	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	body = get_parent()
	max_rot_x = deg2rad(max_rot_x)
	max_rot_y = deg2rad(max_rot_y)
	min_rot_x = deg2rad(min_rot_x)
	min_rot_y = deg2rad(min_rot_y)
	pass # Replace with function body.

func _input(event):
	rotate_mouse(event)
	

func rotate_mouse(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED and rotatepointx != null:
		if horizontal_inverted:
			rotatepointx.rotate_x(deg2rad(event.relative.y))
		else:
			rotatepointx.rotate_x(deg2rad(event.relative.y *- 1))
		if vertical_inverted:
			rotatepointy.rotate_y(deg2rad(event.relative.x))
		else:
			rotatepointy.rotate_y(deg2rad(event.relative.x* -1))

func rotate_yaw(delta):
	var look_amount = Input.get_action_strength(bind_left) - Input.get_action_strength(bind_right)
	look_amount *= delta
	if vertical_inverted:
		look_amount*=-1
	rotatepointy.rotate_y(look_speed_vertical*look_amount)
	if limit_y:
		rotatepointy.rotation.y = clamp(rotatepointy.rotation.y, min_rot_y, max_rot_y)
	pass

func rotate_pitch(delta):
	var look_amount = Input.get_action_strength(bind_up) - Input.get_action_strength(bind_down)
	look_amount *= delta
	if horizontal_inverted:
		look_amount*=-1
	rotatepointx.rotate_x(look_speed_vertical*look_amount)
	if limit_x:
		rotatepointx.rotation.x = clamp(rotatepointx.rotation.x, min_rot_x, max_rot_x)
	pass

func manage_mouse():
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	pass

func _process(delta):
	manage_mouse()
	if rotate_body_x:
		rotatepointx = body
	else:
		rotatepointx = pivot
	if rotate_body_y:
		rotatepointy = body
	else:
		rotatepointy = pivot
	rotate_pitch(delta)
	rotate_yaw(delta)
	pass
