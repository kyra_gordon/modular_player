extends KinematicBody

var velocity = Vector3()

export (float) var gravity = 9.8
export (Vector3) var global_gravity_vector = Vector3.DOWN

#format = acceleration, decceleration, top_speed
export (Dictionary) var movement_modes = {"walk":[10,2.5,10], "crouch":[5,10,15], "sprint":[15,5,30]}
export (bool) var can_move = true
export (float) var coyote_time = .5
var move = 1

var move_mode = "walk"

var airtime = 0

var velbuffer = Vector3()

export (float) var turn_speed = 10

func _ready():
	if can_move:
		move = 1
	else:
		move = 0
	pass # Replace with function body.

func _process(_delta):
	pass

func _physics_process(delta):
	control_movement(delta)
	_move(delta)
	pass

func crouch(_delta):
	pass

func turn_left(delta, amount):
	rotate_x(-amount*delta)
	pass

func turn_right(delta, amount):
	rotate_x(amount*delta)
	pass

var movector = Vector2()

func control_movement(delta):
	$"jump module".call(delta)
	movector.y = Input.get_action_strength("movement_backward") - Input.get_action_strength("movement_forward")
	movector.x = Input.get_action_strength("movement_right") - Input.get_action_strength("movement_left")
	pass

func get_top_speed():
	return move*movement_modes[move_mode][2]
	pass

func get_acceleration():
	return move*movement_modes[move_mode][0]
	pass

func get_decceleration():
	return move*movement_modes[move_mode][1]
	pass

var m = Vector3()

var g = global_gravity_vector*gravity

func _move(delta):
	var nvel = velocity
	var m
	var r
	if movector == Vector2():
		m = g
		r = get_decceleration()
	else:
		m = ((global_transform.basis.z*movector.y+global_transform.basis.x*movector.x)*get_top_speed())
		r = get_acceleration()
	nvel = nvel.linear_interpolate(m,delta*r)
	nvel.y = velocity.y- gravity*delta
	velocity = move_and_slide(nvel+velbuffer,-global_gravity_vector, true,4,deg2rad(45))
	velbuffer = Vector3()
	pass
