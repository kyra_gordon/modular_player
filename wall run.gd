extends Spatial


export (float) var wall_run_speed = 20
export (float) var wall_run_gravity = 1
export (float) var wall_run_stickiness = 10
export (bool) var can_stop_without_falling = false
export (bool) var duration = 5
var gravity_buffer
onready var l = $RayCast_l
onready var r = $RayCast_r
var body
export (bool) var enabled = true
var active = false
var norm = Vector3()

var rays 

func _ready():
	rays = [l,r]
	body = get_parent()
	for i in rays:
		i.add_exception(body)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if active and enabled:
		run(delta)
		pass
	pass

var wall_dir = Vector3()
var last_vel = Vector3()

func check_rays():
	for i in rays:
		if i.is_colliding():
			norm = i.get_collision_normal().rotated(body.global_transform.basis.y,deg2rad(90)).normalized()#get the normal and rotate it
			if norm.dot(global_transform.basis.z)>0:#if it's backwards rotate it 180
				pass
	pass

func run(_delta):
	#check the player is touching the wall and everything, kick them off if they're not or if they change direction
	#if settings are valid for a wall run then set the direction to be at a tangent to the wall
	#move and slide in that direction + the defined gravity direction
	pass

func AI(_delta):
	pass
